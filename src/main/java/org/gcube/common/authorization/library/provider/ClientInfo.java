package org.gcube.common.authorization.library.provider;

import java.io.Serializable;
import java.util.List;

import org.gcube.common.authorization.library.ClientType;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso(value={UserInfo.class, ServiceInfo.class, ExternalServiceInfo.class, ContainerInfo.class})
public abstract class ClientInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public abstract String getId();
		
	public abstract List<String> getRoles();
	
	public abstract ClientType getType();
	
}
