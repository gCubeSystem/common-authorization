package org.gcube.common.authorization.library;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CalledService {

	private String serviceClass;
	private String serviceName;
			
	protected CalledService() {
		super();
	}

	public CalledService(String serviceClass, String serviceName) {
		super();
		this.serviceClass = serviceClass;
		this.serviceName = serviceName;
	}
	
	public String getServiceClass() {
		return serviceClass;
	}

	public String getServiceName() {
		return serviceName;
	}
	
}
