package org.gcube.common.authorization.library;

import java.util.HashMap;
import java.util.Map;

import org.gcube.common.authorization.library.utils.MapAdapter;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;



@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class QualifiersList {

	/**
	 * a map with qualifier as key and token as value 
	 */
	@XmlJavaTypeAdapter(MapAdapter.class)
	Map<String, String> qualifierTokenMap= new HashMap<String, String>();

	
	@SuppressWarnings("unused")
	private QualifiersList(){}
	
	public QualifiersList(Map<String, String> qualifierTokenMap) {
		this.qualifierTokenMap = qualifierTokenMap;
	}

	public Map<String, String> getQualifiers() {
		return qualifierTokenMap;
	}

	@Override
	public String toString() {
		return "QualifiersList [qualifierTokenMap=" + qualifierTokenMap + "]";
	}

	
	
}
