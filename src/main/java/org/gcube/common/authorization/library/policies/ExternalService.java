package org.gcube.common.authorization.library.policies;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ExternalService extends UserEntity {

	@Override
	public UserEntityType getType() {
		return UserEntityType.EXTERNALSERVICE;
	}

	@Override
	public boolean isSubsetOf(UserEntity entity) {
		if (entity.getType()== UserEntityType.EXTERNALSERVICE)
			return entity.getIdentifier()==null || this.getIdentifier().equals(entity.getIdentifier()); 
		else return false;
	}

}
