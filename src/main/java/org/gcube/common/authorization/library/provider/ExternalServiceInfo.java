package org.gcube.common.authorization.library.provider;

import java.util.Collections;
import java.util.List;

import org.gcube.common.authorization.library.ClientType;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ExternalServiceInfo extends ClientInfo{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;
	
	private String generatedBy;
	
	private List<String> roles;
	
	public ExternalServiceInfo(String id, String generatedBy) {
		super();
		this.id = id;
		this.generatedBy = generatedBy;
		this.roles = Collections.emptyList();
	}
	
	public ExternalServiceInfo(String id, String generatedBy, List<String> roles) {
		super();
		this.id = id;
		this.generatedBy = generatedBy;
		this.roles = roles;
	}
	
	protected ExternalServiceInfo() {
		super();
	}

	@Override
	public String getId() {
		return id;
	}
	
	public String getGeneratedBy() {
		return generatedBy;
	}

	@Override
	public List<String> getRoles() {
		return roles;
	}

	@Override
	public ClientType getType() {
		return ClientType.EXTERNALSERVICE;
	}
}
