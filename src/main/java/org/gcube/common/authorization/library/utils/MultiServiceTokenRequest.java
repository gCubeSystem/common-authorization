package org.gcube.common.authorization.library.utils;

import java.util.ArrayList;
import java.util.List;

import org.gcube.common.authorization.library.provider.ServiceInfo;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class MultiServiceTokenRequest {
	
	private List<String> containerTokens= new ArrayList<String>();
	
	private ServiceInfo info;
	
	protected MultiServiceTokenRequest() {}
	
	public MultiServiceTokenRequest(List<String> containerTokens, ServiceInfo info) {
		this.containerTokens = containerTokens;
		this.info = info;
	}

	public List<String> getContainerTokens() {
		return containerTokens;
	}

	public ServiceInfo getInfo() {
		return info;
	}
	
}
