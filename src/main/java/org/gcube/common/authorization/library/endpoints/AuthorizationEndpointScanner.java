package org.gcube.common.authorization.library.endpoints;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.Unmarshaller;

public class AuthorizationEndpointScanner {

	private static Logger log = LoggerFactory.getLogger(AuthorizationEndpointScanner.class);

	private static EndpointsContainer endpoints;

	/**
	 * The path used to find service map configuration files.
	 */
	static final String configurationPattern = ".*\\.authorization";

	/**
	 * Scans the classpath for AuthorizationEndpoints.
	 */
	public static synchronized EndpointsContainer endpoints() {

		if (endpoints==null || endpoints.getEndpoints().size()==0){
			log.trace("starting  authorization endpoint retrieving");
			Map<String, AuthorizationEndpoint> endpointsMap = new HashMap<String, AuthorizationEndpoint>();
			
			try {

				ConfigurationBuilder reflectionConf =  new ConfigurationBuilder().addUrls(ClasspathHelper.forJavaClassPath()).addUrls(ClasspathHelper.forClassLoader()).addClassLoader(AuthorizationEndpointScanner.class.getClassLoader()).setScanners(new ResourcesScanner());
				
				Reflections reflection = new Reflections(reflectionConf); 
	
				Set<String> resources  = reflection.getResources((String input)-> input.endsWith(".authorization"));
				log.info("loading resources {} ", resources);
				
				JAXBContext context = JAXBContext.newInstance(AuthorizationEndpoint.class);
				Unmarshaller um = context.createUnmarshaller();
				
				String defaultInfrastructure = null;
				int defaultInfraPriority= Integer.MAX_VALUE;
				
				for (String resource: resources) {
					log.info("loading {} ", resource);
					URL url = Thread.currentThread().getContextClassLoader().getResource(resource);
					AuthorizationEndpoint endpoint = (AuthorizationEndpoint)um.unmarshal(url);
					if (defaultInfrastructure==null)
						defaultInfrastructure = endpoint.getInfrastructure();
					
					
					if (!endpointsMap.containsKey(endpoint.getInfrastructure()) 
							|| endpointsMap.get(endpoint.getInfrastructure()).getPriority()> endpoint.getPriority()){
						if (resource.startsWith("default") && endpoint.getPriority()<defaultInfraPriority ){
							defaultInfrastructure = endpoint.getInfrastructure();
							defaultInfraPriority = endpoint.getPriority();
						}
						endpointsMap.put(endpoint.getInfrastructure(), endpoint);
					}
										
					log.info("loaded endpoint {} ",endpoint.toString());
					

			   } 
				
				if (endpointsMap.size()==0)
					throw new Exception("no endpoints retreived");
				endpoints = new EndpointsContainer(endpointsMap, defaultInfrastructure);
				log.trace("authorization endpoint retrieving finished");
				
			} catch (Throwable e) {
				log.error("error scanning auth endpoints",e);
				throw new RuntimeException("could not load auth endpoints", e);
			}

		}
		
		return endpoints;
	}
}			