package org.gcube.common.authorization.library.utils;

import java.util.ArrayList;
import java.util.List;

import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ListMapper {

		private List<String> list = new ArrayList<String>();

		protected ListMapper() {}
		
		public ListMapper(List<String> list) {
			super();
			this.list = list;
		}

		public List<String> getList() {
			return list;
		}

		public void setList(List<String> list) {
			this.list = list;
		}
		
}
