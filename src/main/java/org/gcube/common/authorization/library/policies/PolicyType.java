package org.gcube.common.authorization.library.policies;

public enum PolicyType {

	SERVICE, 
	USER
}
